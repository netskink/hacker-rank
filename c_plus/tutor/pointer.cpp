#include <stdio.h>
#include <math.h>

void update(int *a,int *b) {
    // Complete this function  
    int my_a = *a;
    int my_b = *b;
    
    *a = my_a + my_b;
    *b = abs(my_a - my_b);
}

int main() {
    int a, b;
    int *pa = &a, *pb = &b;
    
    scanf("%d %d", &a, &b);
    update(pa, pb);
    printf("%d\n%d", a, b);

    return 0;
}

