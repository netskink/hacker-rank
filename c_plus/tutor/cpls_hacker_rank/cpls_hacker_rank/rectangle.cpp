//
//  rectangle.cpp
//  cpls_hacker_rank
//
//  Created by John Fred Davis on 6/14/18.
//  Copyright © 2018 Netskink Computing. All rights reserved.
//

#include "rectangle.hpp"

#include <iostream>
using namespace std;

Rectangle::Rectangle() {
    width = 1;
    height = 1;
    strcpy(name,"Rectangle");
    
}

Rectangle::Rectangle(int width, int height) {
    this->width = width;
    this->height = height;
    strcpy(name,"Rectangle");
}

Rectangle::Rectangle(const Rectangle &pRect) {
    this->width = pRect.width;
    this->height = pRect.height;
    strcpy(this->name,pRect.name);
}



Rectangle::~Rectangle() {
    cout << "Rectangle destructor" << endl;
}


void Rectangle::set_values(int width,  int height) {
    this->width = width;
    this->height = height;
}

int Rectangle::area() {
    return (width * height);
}


void do_rect_test(void) {
    Rectangle x;
    cout << "default area = " << x.area() << endl;
    cout << "shape name = " << x.get_name() << endl;

    x.set_values(2, 3);
    cout << "area of 2x3 = " << x.area() << endl;

    Rectangle y = x;
    cout << "area of 2x3 copy = " << y.area() << endl;
    cout << "shape name = " << y.get_name() << endl;


}
