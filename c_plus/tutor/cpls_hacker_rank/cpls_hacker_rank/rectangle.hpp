//
//  rectangle.hpp
//  cpls_hacker_rank
//
//  Created by John Fred Davis on 6/14/18.
//  Copyright © 2018 Netskink Computing. All rights reserved.
//

#ifndef rectangle_hpp
#define rectangle_hpp

#include <stdio.h>
#include "shape.hpp"

class Rectangle : public Shape {
    int width;
    int height;
public:
    Rectangle();
    Rectangle(int width, int height);
    Rectangle(const Rectangle &pRect);
    ~Rectangle();
    
    
    void set_values(int width, int height);
    int area(void);
};

void do_rect_test(void);


#endif /* rectangle_hpp */
