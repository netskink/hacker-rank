//
//  vector_stl.hpp
//  cpls_hacker_rank
//
//  Created by John Fred Davis on 6/13/18.
//  Copyright © 2018 Netskink Computing. All rights reserved.
//

#ifndef vector_stl_hpp
#define vector_stl_hpp

#include <stdio.h>

void do_vec_test(void);
void do_vec_vec_test(void);
void do_vec_vec_test2(void);


#endif /* vector_stl_hpp */
