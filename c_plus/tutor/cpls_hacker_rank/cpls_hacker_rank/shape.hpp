//
//  shape.hpp
//  cpls_hacker_rank
//
//  Created by John Fred Davis on 6/14/18.
//  Copyright © 2018 Netskink Computing. All rights reserved.
//

#ifndef shape_hpp
#define shape_hpp

#include <stdio.h>


class Shape {
protected:
    int area;
    char name[20];
public:
    
    void set_name(char *pName);
    char * get_name(void);
};


#endif /* shape_hpp */
