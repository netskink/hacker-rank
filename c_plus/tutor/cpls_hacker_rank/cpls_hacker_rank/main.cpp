//
//  main.cpp
//  cpls_hacker_rank
//
//  Created by John Fred Davis on 6/13/18.
//  Copyright © 2018 Netskink Computing. All rights reserved.
//

#include <iostream>
#include <vector>
#include <iterator>


#include "vector_stl.hpp"
#include "rectangle.hpp"


int main(int argc, const char * argv[]) {
    // insert code here...
    std::cout << "Hello, World!\n";
    
    //do_vec_test();
    //do_vec_vec_test();
    // do_vec_vec_test2();

    do_rect_test();
    
    
    return 0;
}
