//
//  vector_stl.cpp
//  cpls_hacker_rank
//
//  Created by John Fred Davis on 6/13/18.
//  Copyright © 2018 Netskink Computing. All rights reserved.
//

#include "vector_stl.hpp"

#include <iostream>
#include <vector>
#include <iterator>


void do_vec_test(void) {

    // Create a vector containing integers
    std::vector<int> v = {7, 5, 16, 8};

    // Add two more integers to vector
    v.push_back(25);
    v.push_back(13);

    // Iterate and print values of vector
    for(int n : v) {
        std::cout << n << '\n';
    }

}


typedef std::vector<int> vec_int_type;
typedef std::vector<vec_int_type> vec_vec_int_type;

void foo(vec_vec_int_type& param) {
}

void do_vec_vec_test(void) {
    
    // Create a vector containing vectors
    vec_vec_int_type vv;  // a vector of vectors of int
    vec_int_type vi1;   // a vector of ints
    vec_int_type vi2;   // another vector of ints

    // init the first vector of ints
    vi1.push_back(0);
    vi1.push_back(2);
    vi1.push_back(4);

    // init the second vector of ints
    vi2.push_back(1);
    vi2.push_back(3);
    vi2.push_back(5);

    // Add the two vectors to the vector of vectors
    vv.push_back(vi1);
    vv.push_back(vi2);
    
    // dump the vector of vectors
    using Iter = vec_vec_int_type::const_iterator;
    for (Iter it = vv.begin(); it!=vv.end(); ++it) {
        for (int d : *it) {
            std::cout << d << std::endl;
        }
    }
}

void do_vec_vec_test2(void) {
    

    
    
    // First line has two numbers, n=num of arrays, q=num of queries
    // next n lines are the arrays
    //      for each line the first number is the size of the array, the rest are
    //          values of the array
    // After the n array lines is the queries
    // the query will specify which array and which element.  The two numbers are
    // zero based.
    
    int n; // num arrays
    int q; // num queries
    

    // Create a vector containing vectors
    vec_vec_int_type vv;  // a vector of vectors of int

    
    scanf("%d %d", &n, &q);

    // there are n arrays
    int size;  // size of current array
    int x; // array element
    for (int i=0; i<n; i++) {
        vec_int_type vi;   // a vector of ints
        // read an array
        scanf("%d", &size);
        for (int j=0; j<size; j++) {
            scanf(" %d", &x);
            vi.push_back(x);
        }
        vv.push_back(vi);
    }

    ///////////////
    // the next part of the io is the query portion
    struct a_query {
        int which_array;
        int which_element;
    };

    
    
    // There are q queries
    for (int i=0; i<q; i++) {
        vec_int_type vi;   // a vector of ints
                           // read an array
        scanf("%d", &size);
        for (int j=0; j<size; j++) {
            scanf(" %d", &x);
            vi.push_back(x);
        }
        vv.push_back(vi);
    }

    // dump the vector of vectors
//    std::cout << "dump what we have so far" << std::endl;
//    using Iter = vec_vec_int_type::const_iterator;
//    for (Iter it = vv.begin(); it!=vv.end(); ++it) {
//        for (int d : *it) {
//            std::cout << d << std::endl;
//        }
//    }
}
