#include <vector>
#include <iostream>
#include <iterator>

#ifdef orig
int main() {

    std::size_t size{}; // Here we use 'std::size_t' instead of 'int'
                        // because an array size cannot be negative
    std::cin >> size;
    std::vector<int> vect(size);
    for (std::size_t i{}; i < size; ++i)
    	std::cin >> vect[i];

    // One of the many advantages to use C++ STL containers:
    // we can use reverse iterators
    for (auto rit = vect.rbegin(); rit != vect.rend(); ++rit)
        std::cout << *rit << ' ';
    std::cout << std::endl;
    return 0;
}
#endif

#ifdef wtf
int main() {

    std::size_t size{}; // Here we use 'std::size_t' instead of 'int'
                        // because an array size cannot be negative
//    std::cin >> size;
 //   std::vector<int> vect(size);
//    for (std::size_t i{}; i < size; ++i)
//    	std::cin >> vect[i];

    // One of the many advantages to use C++ STL containers:
    // we can use reverse iterators
//    for (auto rit = vect.rbegin(); rit != vect.rend(); ++rit)
//        std::cout << *rit << ' ';
//    std::cout << std::endl;
    return 0;
}
#endif
 
int main()
{
    // Create a vector containing integers
    std::vector<int> v = {7, 5, 16, 8};
 
    // Add two more integers to vector
    v.push_back(25);
    v.push_back(13);
 
    // Iterate and print values of vector
    for(int n : v) {
        std::cout << n << '\n';
    }
}
