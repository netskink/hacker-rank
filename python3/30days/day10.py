#!/usr/bin/python3

#  Task 
# Given a base-10 integer, , convert it to binary (base-2). Then find and print the
# base-10 integer denoting the maximum number of consecutive 's in 's binary
# representation.

#  Input Format
# A single integer, n.

#  Constraints
# 1 <= n <= 10^6

#  Output Format
# Print a single base-10 integer denoting the maximum number of consecutive 1's in
# the binary representation of n


# read the input params
n=int(input().strip())
# convert to binary string
x=bin(n)
#print(x)
#print(type(x))

max_ones=0
cons_ones=0
for i in range(0,len(x)):
    # skip the 0b
    if i < 2:
        continue
    if '1' == x[i]:
        cons_ones+=1
        if cons_ones > max_ones:
            max_ones = cons_ones
    else:
        cons_ones=0
    
    # print(i,x[i])

print(max_ones)


# read from file
#f=open('input8_01.txt','r')
#n = int(f.readline())         
#print(n)

    


# read from file
#f.close()








