#!/usr/bin/python3


# read the input params
mealCost = float(input())
tipPercent = float(input())
taxPercent = float(input())

# read from stdin
#f=open('input2_01.txt','r')
#mealCost = float(f.readline())
#tipPercent = float(f.readline())
#taxPercent = float(f.readline())
#f.close()

print('mealCost = {}'.format(mealCost))
print('tipPercent = {}'.format(tipPercent))
print('taxPercent = {}'.format(taxPercent))

tip=mealCost*tipPercent/100.0
tax=mealCost*taxPercent/100.0
totalCost = mealCost + tip + tax

print('The total meal cost is {}'.format(round(totalCost)))


