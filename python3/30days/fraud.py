#!/usr/bin/python3


# read from file
f=open('input01.txt','r')

# read the input params
# read from stdin
#num_transactions, num_days_to_use = input().split(' ')
# alternate approach
# foo,goo = [int(x) for x in input().split()]
num_transactions, num_days_to_use = f.readline().split(' ')
num_transactions=int(num_transactions)
num_days_to_use=int(num_days_to_use)

#print('input user data')
#print('num_transactions = {}'.format(num_transactions))
#print('num_days_to_use = {}'.format(num_days_to_use))

# read the second line of input parms
# read from stdin
#transactions = input().split(' ')
# read from file
transactions = f.readline().split()
f.close()

#print('transaction = {}'.format(transactions))


# returns 1 if an alert is raised,
# returns 0 is an alert is not raised
def calc_alerts(day1):
    final_day=day1+num_days_to_use
    days = transactions[day1:final_day]
    #print('days = {}'.format(days))


    sorted_days=days
    sorted_days.sort()
    #print('sorted days = {}'.format(sorted_days))
    if (num_days_to_use%2 == 1):
        #print('odd')
        the_median = int(sorted_days[-(-num_days_to_use//2)-1]) # count from 0 using inverse floor to get ceiling

    else:
        #print('even')
        the_first = -(-num_days_to_use//2)-1
        the_second = the_first+1
        #print('the_first {}'.format(the_first))
        #print('the_second {}'.format(the_second))
        the_median = (int(sorted_days[the_first])+int(sorted_days[the_second]))/2

    #print('the_median = {}'.format(the_median))

    the_transaction = int(transactions[final_day])
    #print('the transaction {}'.format(the_transaction))

    if (the_transaction >= 2*the_median):
        #print('alert issued.')
        return 1; # an alert is raised

    return 0; # no alert raised


the_alerts = 0
for i in range(len(transactions)):
    # print('transaction[i={}]={}'.format(i,transactions[i]))
    if i>=num_days_to_use:
        #print(i)
        day1=i-num_days_to_use
        the_alerts = the_alerts + calc_alerts(day1);
        #print('number of alerts = {}'.format(the_alerts))


#day1 = 0
#the_alerts = calc_alerts(day1);
#print('number of alerts = {}'.format(the_alerts))
print(the_alerts)