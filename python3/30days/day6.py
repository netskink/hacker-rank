#!/usr/bin/python3

# Given a string, S, of length N that is indexed from 0 to N-1, 
# print its even-indexed and odd-indexed characters as  2 space-separated 
# strings on a single line (see the Sample below for more detail).

# 0 is considered to be an even index

# The first line contains an integer, T (the number of test cases). 
# Each line i of the T subsequent lines contain a String, S.


## Sample Input
# 2
# Hacker
# Rank


## Sample Output
# Hce akr
# Rn ak

# summary, print the even indexed chars, followed by odd indexed chars
# S[0],S[2],S[4],S[1],S[3],S[5]



# read the input params
T=int(input())


# read from file
#f=open('input6_01.txt','r')
#T = int(f.readline())         

for i in range(0, T):
    # read from file
    #S = f.readline().strip()

    # read from stdin
    S = input().strip()


    #print(i, S)
    str_len = len(S)
    #print('len = ',str_len)
    for j in range(0,str_len):
        if (0==j%2):
            print(S[j],end='')
    print(' ',end='')
    for j in range(0,str_len):
        if (1==j%2):
            print(S[j],end='')
    print('')
    




# read from file
#f.close()








