#!/usr/bin/python3

## Task 
# Given  names and phone numbers, assemble a phone book that maps friends' names
# to their respective phone numbers. You will then be given an unknown number of
# names to query your phone book for. For each name queried, print the associated
# entry from your phone book on a new line in the form name=phoneNumber; if an
# entry for  is not found, print Not found instead.

# Note: Your phone book should be a Dictionary/Map/HashMap data structure.

## Input Format
# 
# The first line contains an integer, n, denoting the number of entries in the
# phone book.  Each of the n subsequent lines describes an entry in the form of
# 2 space-separated values on a single line. The first value is a friend's name,
# and the second value is an 8-digit phone number.

# After the n lines of phone book entries, there are an unknown number of lines of
# queries. Each line (query) contains a name to look up, and you must continue
# reading lines until there is no more input.
# 
# Note: Names consist of lowercase English alphabetic letters and are first names only.

## Constraints
#
# 1 <= n <= 10^5
# 1 <= queires <= 10^5

## Output Format
# 
# On a new line for each query, print Not found if the name has no corresponding
# entry in the phone book; otherwise, print the full name and phoneNumber in the format
# name=phoneNumber.

# Sample Input 
#
# 3
# sam 99912222
# tom 11122222
# harry 12299933
# sam
# edward
# harry

# Sample Output
# sam=99912222
# Not found
# harry=12299933


# read the input params
n=int(input())


# read from file
#f=open('input8_01.txt','r')
#n = int(f.readline())         
#print(n)
a_dict={}

for i in range(0, n):
    # read from file
    #name, phone_number = f.readline().strip().split()

    # read from stdin
    name, phone_number = input().strip().split()


    # print(i, name, phone_number)
    a_dict[name] = phone_number
    
# print(a_dict)

# the rest of the input is for lookup
while True:
    #lookup_name = f.readline().strip()
    lookup_name = input().strip()
    if not lookup_name:
        break
    else:
        if lookup_name in a_dict:
            a_number = a_dict[lookup_name]
            print(lookup_name,'=',a_number,sep='') 
        else:
            print('Not found')
    
    



# read from file
#f.close()








