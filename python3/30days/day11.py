#!/usr/bin/python3

##  Task 
# Calculate the hourglass sum for every hourglass in , then print the maximum
# hourglass sum.

##  Input Format
# There are  lines of input, where each line contains  space-separated integers
# describing 2D Array ; every value in  will be in the inclusive range of  to .

##  Constraints

##  Output Format
# Print the largest (maximum) hourglass sum found in .

# to get maxint
import sys

# read from file
f=open('input11_02.txt','r')

matrix=[]
for line in f:
    row = [int(arr_temp) for arr_temp in line.strip().split(' ')]
    matrix.append(row)

# read from file
f.close()

# print the matrix
#print(matrix)
# Its of form matrix[row][col]
#print(matrix[3][3])

# get size of one dimension. assuming its square matrix.
size = len(matrix)
#print(size)

count=0
max = -sys.maxsize
for i in range(size-2):
    for j in range(size-2):
        #print(i,j)
        sum = 0
        # top of hourglass
        sum += matrix[i][j]
        sum += matrix[i][j+1]
        sum += matrix[i][j+2]
        # middle of hourglass
        sum += matrix[i+1][j+1]
        # bottom of hourglass
        sum += matrix[i+2][j]
        sum += matrix[i+2][j+1]
        sum += matrix[i+2][j+2]

        print('sum[',count,'] =',sum)
        count += 1
        
        if sum > max:
            max = sum


print('max = ',max)



