#!/usr/bin/python3

# first line of input is T number of test cases
# T subsquent lines of integer age denoting age of a person

# 1 <= T <= 4
# -5 <= age <= 30

class Person:

    age=0   # class variable shared by all instances

    def __init__(self,initialAge):
        # Add some more code to run some checks on initialAge
        if (initialAge < 0):
            print('Age is not valid, setting age to 0.')
            self.age = 0    # instance variable unique to each instance
        else:
            self.age = initialAge

    def amIOld(self):
        # Do some computations in here and print out the correct statement to the console
        #
        #  I had to use self.age instead of age
        # age is the global and not the self.age.  
        # adding age to the class makes it a class variable
        # you must use self.age to refer to instance variables.
        if self.age < 13:
            print('You are young.');
        elif self.age >= 13 and self.age < 18:
            print('You are a teenager.');
        else:
            print('You are old.')


    def yearPasses(self):
        # Increment the age of the person in here      
        self.age = self.age + 1




# read the input params
#t = int(input().strip())

# read from file
f=open('input4_01.txt','r')
t = int(f.readline())         
for i in range(0, t):

    # read the input params
    # age = int(input())         

    # read from file
    age = int(f.readline())         
    p = Person(age)  
    p.amIOld()
    for j in range(0, 3):
        p.yearPasses()       
    p.amIOld()
    print("")

# read from file
f.close()


#mealCost = float(f.readline())
#tipPercent = float(f.readline())
#taxPercent = float(f.readline())

#print('n = {}'.format(n))






