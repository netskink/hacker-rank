#!/usr/bin/python3

# Task 
# Write a factorial function that takes a positive integer,  as a parameter and
# prints the result of ( factorial).

# Note: If you fail to use recursion or fail to name your recursive function
# factorial or Factorial, you will get a score of .

# Input Format
# A single integer,  (the argument to pass to factorial).

# Constraints
# 2 <= N <= 12

# Your submission must contain a recursive function named factorial.

# Output Format
# Print a single integer denoting .

def factorial(n):
    #print(n)
    if (1 == n):
        return 1
    return n * factorial(n-1)

# read the input params
N=int(input())


# read from file
#f=open('input8_01.txt','r')
#n = int(f.readline())         
#print(n)

    
print(factorial(N))


# read from file
#f.close()








