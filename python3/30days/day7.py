#!/usr/bin/python3

# first line of input is n, the size of array
# n space seperated ints for array

## Sample Input

# 4
# 1 4 3 2

## Sample Output

# 2 3 4 1




# read the input params
#n = int(input().strip())
#arr = [int(arr_temp) for arr_temp in input().strip().split(' ')]


# read from file
f=open('input7_01.txt','r')
n = int(f.readline())         
arr = [int(arr_temp) for arr_temp in f.readline().strip().split(' ')]

#print(n)
#print(arr)
for i in range(n, 0, -1):
    #print(i)
    print(arr[i-1], end=" ")




# read from file
f.close()








