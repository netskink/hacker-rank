#!/usr/bin/python3


# read the input params
n = int(input().strip())

# read from stdin
#f=open('input2_01.txt','r')
#mealCost = float(f.readline())
#tipPercent = float(f.readline())
#taxPercent = float(f.readline())
#f.close()

#print('n = {}'.format(n))

if (n%2==1):
    print('Weird')
else: 
    if (n>=2 and n<=5):
        print('Not Weird')
    elif (n>=6 and n<=20):
        print('Weird')
    else:
        print('Not Weird')

